$(document).ready(function(){
    if(sessionStorage.getItem('fullname') != null){
        $('#profile_fullname').html(sessionStorage.getItem('fullname').toString());
        $('#profile_email').html(sessionStorage.getItem('email').toString());
        $('#authform').hide();
    }else {
         $('#profile').hide();
    }
});
var signIn = function () {
  $.ajax({
    type: "POST",
    beforeSend: function(request) {
      request.setRequestHeader("email", document.getElementById('email').value.trim());
      request.setRequestHeader("password", document.getElementById('password').value.trim());
    },
    url: urlEndpoint + '/signin',
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: loginCallback,
    error: OnErrorLogin
  });
};

var signUp = function (){
    $.ajax({
        type: "POST",
        beforeSend: function(request) {
            request.setRequestHeader("fullname", document.getElementById('fullname').value.trim());
            request.setRequestHeader("email", document.getElementById('email').value.trim());
            request.setRequestHeader("password", document.getElementById('password').value.trim());
        },
        url: urlEndpoint + '/signup',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: loginCallback,
        error: OnErrorLogin
    });
};

var resetPassword = function () {
    alert("Coming soon...")
}

var OnErrorLogin = function (data) {
  if (data.readyState === 0) {
    document.getElementById('notificationDiv').className = 'alert alert-danger alert-dismissible';
    document.getElementById('notification').innerText = '*ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง';
  } else {
    alert( 'มีผิดพลาดกรุณาลองใหม่อีกครั้ง --> <br/>' + JSON.stringify(data));
  }
};

var loginCallback =function(data, textStatus, request){
  console.log(data.data);
    $('#authform').hide();
    $('#profile_fullname').html(data.data.fullname);
    $('#profile_email').html(data.data.email);
    $('#profile').show();
    sessionStorage.setItem('fullname',data.data.fullanme);
    sessionStorage.setItem('email',data.data.email);
  if (data.status) {

    // sessionStorage.setItem("staffId", data._id);
    //sessionStorage.setItem("token",  request.getResponseHeader('Authorization'));

    var url = urlEndpoint + 'mongodb/cores/role_db/query';
    var obj = {
      "query":{
        "application": "cct",
        "user": document.getElementById('username').value.trim()
      }
    };
    Post(url, obj, getUserDataCallback, OnErrorLogin);
  } else {
    //document.getElementById('notificationDiv').className = 'alert alert-danger alert-dismissible';

    if (data.message === 'Username Deactived') {
      //document.getElementById('notification').innerText = 'ผู้ใช้ นี้ถูกระงับการใช้งาน';
    } else {
      //document.getElementById('notification').innerText = '*ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง';
    }
  }
};

var Post = function (url, obj, onSuccess, onError) {
  $.ajax({
    type: "POST",
    beforeSend: function(request) {
      request.setRequestHeader("Authorization", getToken());
    },
    url: url,
    data: JSON.stringify(obj),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: onSuccess,
    error: onError
  });
};

var signOut = function () {
  sessionStorage.clear();
  window.location = "/index.html";
  $('#authform').show();
  $('#profile').hide();
};